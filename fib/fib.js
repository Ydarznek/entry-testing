'use strict';

const n = prompt('Input Number N of Fibonacci sequence to take value of Nth item')
const fib = [0, 1]; // 0 and 1st

for (let i = 2; i <= n; i ++) fib[i] = fib[i - 1] + fib[i - 2];

console.log(fib[n])
alert (n + ' item is equal ' + fib[n])